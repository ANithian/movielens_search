package org.apache.solr.search;

import org.apache.lucene.queries.function.FunctionValues;

/**
 * Place safe math functions here. The slog is the one defined in the zvents_functions.xml as a way to load this class
 * and bootstrap other safe functions that may need to be added. Currently the default Solr functions could return infinity if invalid arguments are passed (such as log(0)).
 *
 *
 */
public class SafeMathFunctions extends DoubleParser
{
    public SafeMathFunctions()
    {
        super("slog");
    }

    @Override
    public double func(int doc, FunctionValues vals) {
      return Math.log(1+vals.doubleVal(doc));
    }

    static
    {
        /*
        addParser(new DoubleParser("sslog") {
            @Override
            public double func(int doc, FunctionValues vals) {
              return Math.log10(vals.doubleVal(doc));
            }
          });
          */
    }
}
