package org.kiji.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.hokiesuns.movielens.avro.MovieRating;
import org.hokiesuns.movielens.avro.SearchBoosts;

import org.kiji.schema.Kiji;
import org.kiji.schema.KijiCell;
import org.kiji.schema.KijiDataRequest;
import org.kiji.schema.KijiDataRequestBuilder.ColumnsDef;
import org.kiji.schema.KijiRowData;
import org.kiji.schema.KijiTable;
import org.kiji.schema.KijiTableReader;
import org.kiji.schema.KijiTableWriter;
import org.kiji.schema.KijiURI;
import org.kiji.search.KijiSearchComponent;

public class UserInfoServlet extends HttpServlet {

  /**
   *
   */
  private static final long serialVersionUID = 5140297987519818645L;

  private KijiSearchComponent mKijiSearchComp = new KijiSearchComponent();

  private KijiURI uri = KijiURI.newBuilder("kiji://.env/movies").build();
  private Kiji kiji;

  public UserInfoServlet() {
    try {
      kiji = Kiji.Factory.open(uri);
    } catch (IOException ioe) {
      ioe.printStackTrace();
    }
  }

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException,
      IOException {
    resp.setContentType("text/html");
    long userId = Long.parseLong(req.getParameter("user"));
    String action = req.getParameter("action");

    KijiTable table = kiji.openTable("users");

    PrintWriter outputWriter = new PrintWriter(resp.getOutputStream());

    String cats = req.getParameter("cats");

    // Lazy... :-)
    if (cats == null) {
      cats = "";
    }

    List<String> catList = Lists.newArrayList(cats.split(","));

    if (action.equals("click")) {
      // A click is a rating of "3"?
      long movieId = Long.parseLong(req.getParameter("movie_id"));

      MovieRating rating = MovieRating
          .newBuilder()
          .setMovieId(movieId)
          .setRating(3)
          .setMovieCats(catList)
          .build();

      KijiTableWriter writer = table.openTableWriter();
      writer.put(table.getEntityId(userId), "interactions", "clicks", rating);
      writer.close();

      incrementCategories(catList, 3);
    }
    else if (action.equals("watch")) {
      long movieId = Long.parseLong(req.getParameter("movie_id"));

      // A watch is a rating of "4"?
      MovieRating rating = MovieRating
          .newBuilder()
          .setMovieId(movieId)
          .setRating(4.0)
          .setMovieCats(catList)
          .build();

      KijiTableWriter writer = table.openTableWriter();
      writer.put(table.getEntityId(userId), "interactions", "watches", rating);
      writer.close();
      incrementCategories(catList, 4);
    }
    else if (action.equals("rate")) {
      long movieId = Long.parseLong(req.getParameter("movie_id"));
      double userRating = Double.parseDouble(req.getParameter("rating"));
      MovieRating rating = MovieRating
          .newBuilder()
          .setMovieId(movieId)
          .setRating(userRating)
          .setMovieCats(Lists.newArrayList(cats.split(",")))
          .build();

      KijiTableWriter writer = table.openTableWriter();
      writer.put(table.getEntityId(userId), "interactions", "ratings", rating);
      writer.close();

      incrementCategories(catList, (int)userRating);
    }
    else if (action.equals("recent_ratings"))
    {
      Map<String, Integer> catPrefs = Maps.newHashMap();

      KijiTableReader reader = table.openTableReader();
      KijiDataRequest request = KijiDataRequest
          .builder()
          .addColumns(ColumnsDef
              .create()
              .withMaxVersions(10)
              .add("interactions", "ratings")
              .add("interactions", "clicks")
              .add("interactions", "watches")
          )
          .build();
      KijiRowData row = reader.get(table.getEntityId(userId), request);
      printInteractions(row, userId, "ratings", outputWriter, catPrefs);
      printInteractions(row, userId, "clicks", outputWriter, catPrefs);
      printInteractions(row, userId, "watches", outputWriter, catPrefs);

      outputWriter.println("<h3>Summary</h3>");
      outputWriter.println(catPrefs);

      SearchBoosts boosts = mKijiSearchComp.computeBoosts(userId);
//      List<CategoryBoost> boosts = mKijiSearchComp.computeBoosts(userId, 3);
      outputWriter.println("<h3>Category Preferences</h3>");
      outputWriter.println(boosts);

      reader.close();
    }
    kiji.release();
    outputWriter.flush();
  }

  private void incrementCategories(List<String> categories, int rating) throws IOException {
    KijiTable cats = kiji.openTable("categories");
    KijiTableWriter writer = cats.openTableWriter();
    for (String s : categories) {
      writer.increment(cats.getEntityId(s), "stats", "num_ratings", 1);
      writer.increment(cats.getEntityId(s), "stats", "sum_ratings", rating);
      writer.increment(cats.getEntityId(s), "stats", "sum_squared_ratings", rating * rating);
    }
    writer.close();
  }

  private void printInteractions(KijiRowData row, long userId, String interactionType,
      PrintWriter outputWriter, Map<String, Integer> summaryMap) throws IOException {
    Iterable<KijiCell<MovieRating>> it = row.asIterable("interactions", interactionType);
    outputWriter.println("<h3> Recent " + interactionType + " for user " + userId + "</h3>");
    outputWriter.println("<ul>");
    for (KijiCell<MovieRating> r : it) {
      MovieRating rating = r.getData();
      String solrUrl = "/select?q=id:" + rating.getMovieId();
      outputWriter.println("<li> <a href='" + solrUrl + "'>" + rating.getMovieId()
          + "</a> - User Rating: " + rating.getRating());
      outputWriter.println("<ul>");
      for (String cat : r.getData().getMovieCats()) {
        outputWriter.println("<li>" + cat + "- Avg Rating: " + mKijiSearchComp.getAvgRating(cat)
            + "</li>");
        Integer count = summaryMap.get(cat);
        if (count == null) {
          count = 0;
        }
        summaryMap.put(cat, count + 1);
      }
      outputWriter.println("</li></ul>");
    }
    outputWriter.println("</ul>");
  }
}
