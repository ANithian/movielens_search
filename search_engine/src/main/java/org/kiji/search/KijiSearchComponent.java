package org.kiji.search;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.google.common.collect.Maps;
import org.apache.solr.common.params.ModifiableSolrParams;
import org.apache.solr.common.params.SolrParams;
import org.apache.solr.handler.component.QueryComponent;
import org.apache.solr.handler.component.ResponseBuilder;
import org.hokiesuns.movielens.avro.MovieRating;
import org.hokiesuns.movielens.avro.SearchBoosts;
import org.hokiesuns.movielens.scoring.SearchScoringFunction;

import org.kiji.mapreduce.kvstore.KeyValueStoreReader;
import org.kiji.schema.Kiji;
import org.kiji.schema.KijiCell;
import org.kiji.schema.KijiColumnName;
import org.kiji.schema.KijiDataRequest;
import org.kiji.schema.KijiDataRequestBuilder.ColumnsDef;
import org.kiji.schema.KijiRowData;
import org.kiji.schema.KijiTable;
import org.kiji.schema.KijiTableReader;
import org.kiji.schema.KijiURI;
import org.kiji.scoring.CounterManager;
import org.kiji.scoring.FreshKijiTableReader;
import org.kiji.scoring.FreshenerContext;
import org.kiji.scoring.FreshenerSetupContext;
import org.kiji.scoring.ScoreFunction.TimestampedValue;

public class KijiSearchComponent extends QueryComponent {

  private static final Map<String, Float[]> categoryMap = getCategoryMap("category_map.tsv");
  private Kiji mKiji;
  private KijiTable mKijiTable;
  private FreshKijiTableReader mKijiReader;
  private KijiDataRequest mBoostRequest = KijiDataRequest.create("boosts", "search_algo_1");

  public static class CategoryBoost implements Comparable<CategoryBoost> {

    private String category;
    private int numObs;
    private float totalScore;

    private float zScore;

    public CategoryBoost(String category) {
      super();
      this.category = category;
    }

    public void setZScore(float zscore) {
      this.zScore = zscore;
    }

    public float getAverageRating() {
      return totalScore / numObs;
    }

    public void submitRating(float rating) {
      numObs++;
      totalScore += rating;
    }

    public int compareTo(CategoryBoost arg0) {
      // TODO Auto-generated method stub
      return Float.compare(Math.abs(this.zScore), Math.abs(arg0.zScore)) * -1;
    }

    @Override
    public String toString() {
      return "CategoryBoost [category=" + category + ", numObs=" + numObs + ", totalScore="
          + totalScore + ", zScore=" + zScore + "]";
    }
  }

  @Override
  public void process(ResponseBuilder rb) throws IOException {
    // TODO Auto-generated method stub
    super.process(rb);
  }

  private static class DummyFreshenerContext implements FreshenerSetupContext, FreshenerContext {

    Map<String,String> params = Maps.newHashMap();

    public DummyFreshenerContext() {
      params.put("category.uri", "kiji://bento:2181/movies");
    }

    @Override
    public KijiColumnName getAttachedColumn() {
      // TODO Auto-generated method stub
      return null;
    }

    @Override
    public String getParameter(String arg0) {
      // TODO Auto-generated method stub
      return params.get(arg0);
    }

    @Override
    public Map<String, String> getParameters() {
      // TODO Auto-generated method stub
      return params;
    }

    @Override
    public <K, V> KeyValueStoreReader<K, V> getStore(String arg0) throws IOException {
      // TODO Auto-generated method stub
      return null;
    }

    @Override
    public KijiDataRequest getClientRequest() {
      // TODO Auto-generated method stub
      return null;
    }

    @Override
    public CounterManager getCounterManager() {
      // TODO Auto-generated method stub
      return null;
    }

  }

  public void prepare(ResponseBuilder rb) throws IOException {
    SolrParams origParams = rb.req.getParams();
    String userId = origParams.get("user");
    if (userId != null && !userId.isEmpty()) {
      // Here transform the query into something that is based on user preferences from a Kiji
      // table.
      ModifiableSolrParams params = new ModifiableSolrParams(rb.req.getParams());

      SearchBoosts boosts = computeBoosts(Long.parseLong(userId));
      if (boosts != null && !boosts.getBoosts().isEmpty()) {
        String query = params.get("q");
        params.remove("q");
        params.set("qq", query);
        for (Entry<String, String> e : boosts.getBoosts().entrySet()) {
          params.set(e.getKey(), e.getValue());
        }
        rb.req.setParams(params);
      }
    }
    super.prepare(rb);
  }

  public SearchBoosts computeBoosts(long userId) throws IOException {
    SearchScoringFunction fn = new SearchScoringFunction();
    FreshenerSetupContext ctx = new DummyFreshenerContext();
    fn.setup(ctx);

    KijiRowData data = mKijiReader.get(mKijiTable.getEntityId(userId), fn.getDataRequest((FreshenerContext)ctx));
    TimestampedValue<SearchBoosts> val = fn.score(data, (FreshenerContext)ctx);

    SearchBoosts boosts = val.getValue();
    return boosts;
  }

  public void Freshprepare(ResponseBuilder rb) throws IOException {
    SolrParams origParams = rb.req.getParams();
    String userId = origParams.get("user");
    if (userId != null && !userId.isEmpty()) {
      // Here transform the query into something that is based on user preferences from a Kiji
      // table.
      ModifiableSolrParams params = new ModifiableSolrParams(rb.req.getParams());
      KijiRowData data = mKijiReader.get(mKijiTable.getEntityId(userId), mBoostRequest);
      SearchBoosts boosts = data.getMostRecentValue("boosts", "search_algo_1");
      if (boosts != null) {
        String query = params.get("q");
        params.remove("q");
        params.set("qq", query);
        for (Entry<String, String> e : boosts.getBoosts().entrySet()) {
          params.set(e.getKey(), e.getValue());
        }
      }
    }
  }

  public void Oldrepare(ResponseBuilder rb) throws IOException {
    SolrParams origParams = rb.req.getParams();
    String userId = origParams.get("user");
    if (userId != null && !userId.isEmpty()) {
      // Here transform the query into something that is based on user preferences from a Kiji
      // table.
      ModifiableSolrParams params = new ModifiableSolrParams(rb.req.getParams());
      List<CategoryBoost> boosts = computeBoosts(Long.parseLong(userId), 3);
      if (boosts != null) {
        String query = params.get("q");
        params.remove("q");
        params.set("qq", query);
        params.set("q", "{!boost b=$b defType=edismax v=$qq}");
        // b=sum(1,sum(product(query($cat1),1.482),product(query($cat2),0.1199),product(query($cat3),1.448)))&cat1=category:Drama&cat2=category:Comedy&cat3=category:Action
        StringBuilder boostBuilder = new StringBuilder();
        boostBuilder.append("sum(1,");
        boostBuilder.append("sum(");
        for (int i = 0; i < boosts.size(); i++) {
          CategoryBoost boost = boosts.get(i);
          // Set the category name params.
          params.set("cat" + (i + 1), "category:" + boost.category);
          // sum(
          // product(query($cat1),1.482),product(query($cat2),0.1199),product(query($cat3),1.448) )
          boostBuilder.append("product(query($cat" + (i + 1) + ")," + boost.zScore + ")");
          if (i < boosts.size() - 1) {
            boostBuilder.append(",");
          }
        }
        boostBuilder.append("))");
        params.set("b", boostBuilder.toString());
        System.out.println("Category boosts = " + boosts);
        System.out.println("New params = " + params);
      }
      rb.req.setParams(params);
    }
    super.prepare(rb);
  }

  public KijiSearchComponent() {
    // TODO Auto-generated constructor stub
    KijiURI uri = KijiURI.newBuilder("kiji://bento:2181/movies").build();
    try {
      mKiji = Kiji.Factory.open(uri);
      mKijiTable = mKiji.openTable("users");
      mKijiReader = FreshKijiTableReader.Builder
          .create()
          .withTable(mKijiTable)
          .build();
    } catch (IOException ioe) {
      // TOOD: Crappy I know.. will fix later.
      ioe.printStackTrace();
    }
  }

  public List<CategoryBoost> computeBoosts(long userId, int numToReturn) {
    KijiURI uri = KijiURI.newBuilder("kiji://bento:2181/movies").build();
    Map<String, CategoryBoost> boostsMap = Maps.newHashMap();
    List<CategoryBoost> sortedBoosts = null;
    int iNumTotalObs = 0;
    try {
      Kiji kiji = Kiji.Factory.open(uri);
      KijiTable table = kiji.openTable("users");
      KijiTableReader reader = table.openTableReader();
      KijiDataRequest request = KijiDataRequest
          .builder()
          .addColumns(ColumnsDef
              .create()
              .withMaxVersions(10)
              .add("interactions", "ratings")
          // .add("interactions", "clicks")
          // .add("interactions", "watches")
          )
          .build();
      KijiRowData row = reader.get(table.getEntityId(userId), request);
      iNumTotalObs += updateBoosts(row, "ratings", boostsMap);
      // iNumTotalObs+=updateBoosts(row, "clicks", boostsMap);
      // iNumTotalObs+=updateBoosts(row, "watches", boostsMap);

      // Now go through each category and compute the z-score
      for (CategoryBoost cat : boostsMap.values()) {
        Float[] stats = categoryMap.get(cat.category);
        float zScore = ((cat.getAverageRating() - stats[1]) / stats[2])
            * (cat.numObs * 1.0f / iNumTotalObs * 1.0f);
        cat.setZScore(zScore);
      }
      List<CategoryBoost> boosts = new ArrayList<KijiSearchComponent.CategoryBoost>(
          boostsMap.values());
      Collections.sort(boosts);
      if (numToReturn > boosts.size()) {
        sortedBoosts = boosts;
      } else {
        sortedBoosts = boosts.subList(0, numToReturn);
      }
      kiji.release();
    } catch (IOException ioe) {

    }

    return sortedBoosts;
  }

  private int updateBoosts(KijiRowData row, String qualifier, Map<String, CategoryBoost> boostsMap)
      throws IOException {
    int iNumObs = 0;
    Iterable<KijiCell<MovieRating>> it = row.asIterable("interactions", qualifier);
    for (KijiCell<MovieRating> r : it) {
      MovieRating rating = r.getData();
      for (String cat : rating.getMovieCats()) {
        CategoryBoost boost = boostsMap.get(cat);
        if (boost == null) {
          boost = new CategoryBoost(cat);
          boostsMap.put(cat, boost);
        }
        boost.submitRating(rating.getRating().floatValue());
        iNumObs++;
      }
    }

    return iNumObs;
  }

  public float getAvgRating(String cat) {
    return categoryMap.get(cat)[1];
  }

  // Adventure 2121074 3.493621156074708 1.052911290591804 (num_obs, avg, std_dev)
  private static Map<String, Float[]> getCategoryMap(String inputFile) {
    Map<String, Float[]> catMap = Maps.newHashMap();
    try {
      BufferedReader reader = new BufferedReader(new FileReader(new File("category_map.tsv")));
      String line = reader.readLine();
      while (line != null) {
        String[] parts = line.split("\t");
        Float[] stats = { Float.parseFloat(parts[1]), Float.parseFloat(parts[2]),
            Float.parseFloat(parts[3]) };
        catMap.put(parts[0], stats);
        line = reader.readLine();
      }
      reader.close();
    } catch (IOException ioe) {
    }

    return catMap;
  }
}
