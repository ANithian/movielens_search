#!/bin/bash

if [ $# -lt 1 ]; then
  echo "Usage: ./setup.sh [setup| ((load|all) <Product Input File> <HDFS base dir>)]"
  exit 1
fi

case "$1" in
  load|setup|all)
    ;;
  *)
    echo "Usage: ./setup.sh [setup| ((load|all) <Product Input File> <HDFS base dir>)]"
    exit 1
esac

if [[ ("$1" == "load" || "$1" == "all") && ( $# -lt 3 ) ]]; then
  echo "Specify path to movielens data and the hdfs location for both input and output"
  echo "Usage: ./setup.sh (load|all) <Product Input File> <HFile Output>"
  exit 1
fi

#Build up the classpath
mvn dependency:build-classpath -Dmdep.cpFile=classpath.txt -DincludeScope=runtime -DexcludeArtifactIds="scala-library,scala-compiler,hadoop-common"

export KIJI=${KIJI:-"kiji://bento:2181/movies"}
export KIJI_CLASSPATH=$(cat classpath.txt):$PWD/target/classes

export INPUT_FILE=$2
export HDFS_BASE_DIR=$3
export HFILE_OUTPUT=${HDFS_BASE_DIR}/hfile_output
export PRODUCT_TABLE=product
export KIJI_JAVA_OPTS="-Xmx4096m -Dmapred.child.java.opts=-Xmx4096m"

mvn clean package -DskipTests
MOVIE_JAR=$(find target -maxdepth 1 -name "*-SNAPSHOT.jar")

if [[ ("$1" == "setup" || "$1" == "all") ]]; then
  kiji install --kiji=${KIJI}
  if [ -e kiji_movies.dat ]; then
    # Restore instance from metadata
    echo "Restoring from backup.."
    kiji metadata --restore=kiji_movies.dat --kiji=${KIJI} --interactive=false --schemas=true
    kiji metadata --restore=kiji_movies.dat --kiji=${KIJI} --interactive=false --schemas=false
  else
    # Create instance and tables
    for ddl in $(find src/main/resources -name "*.ddl")
    do
      kiji-schema-shell --kiji=${KIJI} --file=${ddl}
    done
    rm -f kiji_movies.dat
    kiji metadata --backup=kiji_movies.dat --kiji=${KIJI} --interactive=false
  fi
  if [[ (( "$1" == "setup" )) ]]; then
    exit
  fi
fi

hadoop dfs -mkdir ${HFILE_OUTPUT}

# PRODUCTS
hadoop dfs -test -e ${HFILE_OUTPUT}/bkup/users

if [ $? -ne 0 ]; then
  hadoop dfs -put ${INPUT_FILE} ${HDFS_BASE_DIR}

  express job ${MOVIE_JAR} \
  org.hokiesuns.movielens.loader.UsersTableBulkLoader \
  --libjars "${KIJI_CLASSPATH}" \
  --output ${KIJI}/users \
  --category-output ${KIJI}/categories \
  --hfile-output ${HFILE_OUTPUT}/bkup/data \
  --input ${HDFS_BASE_DIR}/input \
  --aux-output ${HDFS_BASE_DIR}/aux-output \
  --hdfs
fi

hadoop dfs -rmr ${HFILE_OUTPUT}/data
hadoop dfs -cp ${HFILE_OUTPUT}/bkup/data ${HFILE_OUTPUT}/data
hadoop dfs -chmod -R 777 ${HFILE_OUTPUT}/data
kiji bulk-load --table=${KIJI}/users --hfile=${HFILE_OUTPUT}/data/ratings/hfiles
kiji bulk-load --table=${KIJI}/users --hfile=${HFILE_OUTPUT}/data/tags/hfiles
kiji bulk-load --table=${KIJI}/categories --hfile=${HFILE_OUTPUT}/data/categories/hfiles

