package org.hokiesuns.movielens.loader

import com.twitter.scalding.Job
import com.twitter.scalding.Args
import com.twitter.scalding.TextLine
import com.google.common.collect.Maps
import org.kiji.express.flow.framework.hfile.HFileKijiOutput
import scala.collection.immutable.Map
import org.kiji.express.flow.KijiJob
import org.kiji.express.flow.KijiJob
import org.kiji.express.flow.EntityId

//UserID::MovieID::Rating::Timestamp
class RatingKVStoreLoader(args: Args) extends KijiJob(args) {

  val MAX_TIMESTAMP = 1231131736L

  // Read in two files, ratings and tags.
  // Then decay the ratings and have a decayed rating and regular rating
  // join that with the tags. Emit cells to HBase HFile.

  //15::4973::excellent!::1215184630
  //  TextLine("input/tags.dat")
  //    .read
  //    .mapTo('line -> ('entityId, 'movie_id, 'tag, 'timestamp)) { line: String =>
  //      val parts = line.split("::")
  //      (EntityId(parts(0).toLong), parts(1), parts(2), parts(3).toLong)
  //    }
  //    .write(HFileKijiOutput(args("output"),
  //      args("hfile-output"),
  //      'timestamp,
  //      Map('tag -> ColumnRequestOutput("tag", useDefaultReaderSchema = true, qualifierSelector = Some("movie_id")))))

  TextLine("input/ratings.dat")
    .mapTo('line -> ('entityId, 'movie_id, 'rating, 'timestamp)) { line: String =>
      val parts = line.split("::")
      (EntityId(parts(0).toLong), parts(1), parts(2).toDouble, parts(3).toLong)
    }
    .map(('rating, 'timestamp) -> 'rating_decayed) { rt: (Double, Long) =>
      val (rating, timestamp) = rt
      // Let's say that your movie rating preference decays by 10% each year.
      val years = (MAX_TIMESTAMP - timestamp) / (365 * 86400)
      val newRating = rating * Math.pow((9.0 / 10), years)
      newRating.toFloat
    }
    //    .groupBy('entityId) { _.size('num_ratings).average('rating_decayed -> 'avg_decayed_rating).average('rating -> 'avg_rating) }
    //    .filter('avg_decayed_rating) { rating: Double => rating > 0.5 }
//    .write(HFileKijiOutput(args("output"),
//      args("hfile-output"),
//      'timestamp,
//      Map('rating -> ColumnRequestOutput("rating", useDefaultReaderSchema = true, qualifierSelector = Some("movie_id")),
//        'rating_decayed -> ColumnRequestOutput("decayed_rating", useDefaultReaderSchema = true, qualifierSelector = Some("movie_id")) //        'avg_rating -> ColumnRequestOutput("info:avg_rating", useDefaultReaderSchema = true),
//        //        'num_ratings -> ColumnRequestOutput("info:num_ratings", useDefaultReaderSchema = true),
//        //        'avg_decayed_rating -> ColumnRequestOutput("info:avg_decayed_rating", useDefaultReaderSchema = true)
//        )))

}