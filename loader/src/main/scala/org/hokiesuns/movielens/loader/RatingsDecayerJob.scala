package org.hokiesuns.movielens.loader

import com.twitter.scalding.Job
import com.twitter.scalding.Args
import com.twitter.scalding.TextLine

//UserID::MovieID::Rating::Timestamp
class RatingsDecayerJob(args: Args) extends Job(args) {

  val MAX_TIMESTAMP = 1231131736L

  TextLine(args("input"))
    .read
    .mapTo('line -> ('user_id, 'movie_id, 'rating, 'timestamp)) { line: String =>
      val parts = line.split("::")
      (parts(0), parts(1), parts(2).toDouble, parts(3).toLong)
    }
    .map(('rating, 'timestamp) -> 'rating_decayed) { rt: (Double, Long) =>
      val (rating, timestamp) = rt
      // Let's say that your movie rating preference decays by 10% each year.
      val years = (MAX_TIMESTAMP - timestamp) / (365 * 86400)
      val newRating = rating * Math.pow((9.0 / 10), years)
      newRating
    }
    .groupBy('movie_id) { _.size('num_ratings).average('rating_decayed -> 'avg_decayed_rating) }
    .filter('avg_decayed_rating) { rating: Double => rating > 0.5 }
    .project('movie_id, 'num_ratings, 'avg_decayed_rating)
    .write(TextLine(args("output")))
}