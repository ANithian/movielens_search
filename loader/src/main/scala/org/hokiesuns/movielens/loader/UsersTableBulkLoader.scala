package org.hokiesuns.movielens.loader

import com.twitter.scalding.Job
import com.twitter.scalding.Args
import com.twitter.scalding.TextLine
import com.google.common.collect.Maps
import org.kiji.express.flow.framework.hfile.HFileKijiOutput
import scala.collection.immutable.Map
import org.kiji.express.flow.KijiJob
import org.kiji.express.flow.KijiJob
import org.kiji.express.flow.EntityId
import org.kiji.express.flow.ColumnOutputSpec
import org.kiji.express.flow.QualifiedColumnOutputSpec
import org.kiji.express.flow.ColumnFamilyOutputSpec
import org.hokiesuns.movielens.avro.MovieRating
import scala.collection.JavaConverters._
import org.hokiesuns.movielens.avro.MovieTag

//UserID::MovieID::Rating::Timestamp
class UsersTableBulkLoader(args: Args) extends KijiJob(args) {

  val MAX_TIMESTAMP = 1231131736L

  // Read in two files, ratings and tags.
  // Then decay the ratings and have a decayed rating and regular rating
  // join that with the tags. Emit cells to HBase HFile.
  val movies = TextLine("%s/movies.dat".format(args("input")))
    .read
    .mapTo('line -> ('movie_id, 'cats)) { line: String =>
      val parts = line.split("::")
      (parts(0).toLong, parts(2))
    }

  //1::122::5::838985046 (user, movie, rating, timestamp)
  val movie_ratings = TextLine("%s/ratings.dat".format(args("input")))
    .read
    .mapTo('line -> ('entityId, 'user_id, 'movie_id, 'rating, 'timestamp)) { line: String =>
      val parts = line.split("::")
      (EntityId(parts(0).toLong), parts(0).toLong, parts(1).toLong, parts(2).toFloat, parts(3).toLong)
    }
    .map(('rating, 'timestamp) -> 'decayed_rating) { rt: (Double, Long) =>
      val (rating, timestamp) = rt
      // Let's say that your movie rating preference decays by 10% each year.
      val years = (MAX_TIMESTAMP - timestamp) / (365 * 86400)
      val newRating = rating * Math.pow((9.0 / 10), years)
      newRating.toFloat
    }
    .joinWithSmaller(('movie_id, 'movie_id), movies)

  val movie_tags = TextLine("%s/tags.dat".format(args("input")))
    .read
    .mapTo('line -> ('entityId, 'movie_id, 'tag, 'timestamp)) { line: String =>
      val parts = line.split("::")
      (EntityId(parts(0).toLong), parts(1).toLong, parts(2), parts(3).toLong)
    }
    .joinWithSmaller(('movie_id, 'movie_id), movies)

  val user_ratings = movie_ratings
    .flatMap('cats -> 'category) { catList: String => catList.split("\\|") }
    .groupBy(('user_id, 'category)) { _.sizeAveStdev('rating -> ('num_ratings, 'avg_rating, 'stddev_rating)) }
    .filter('num_ratings) { num_ratings: Int => num_ratings > 5 }
    .project('user_id, 'category, 'num_ratings, 'avg_rating)

  val general_ratings = movie_ratings
    .flatMap('cats -> 'category) { catList: String => catList.split("\\|") }
    // Need num, sum, sum_squared
    .map('rating -> 'rating_sq) { rating: Long => rating * rating }
    .groupBy('category) {
      _.size('num_ratings)
        .sum[Long]('rating -> 'sum_ratings)
        .sum[Long]('rating_sq -> 'sum_sq_rating)
    }
    .filter('num_ratings) { num_ratings: Int => num_ratings > 5 }
    .project('category, 'num_ratings, 'sum_ratings, 'sum_sq_rating)

  // Write stuff out now.
  user_ratings.write(TextLine("%s/user_rating_info".format(args("aux-output"))))
  //  general_ratings.write(TextLine("%s/general_rating_info".format(args("aux-output"))))

  movie_ratings
    .groupBy('movie_id) { _.size('num_ratings).average('decayed_rating -> 'avg_decayed_rating) }
    .filter('avg_decayed_rating) { rating: Double => rating > 0.5 }
    .project('movie_id, 'num_ratings, 'avg_decayed_rating)
    .write(TextLine("%s/decayed_ratings".format(args("aux-output"))))

  movie_ratings
    .map(('movie_id, 'cats, 'rating) -> 'ratingRecord) { tuple: (Long, String, Float) =>
      ratingsToRecord(tuple)
    }
    .map(('movie_id, 'cats, 'decayed_rating) -> 'decayedRatingRecord) { tuple: (Long, String, Float) =>
      ratingsToRecord(tuple)
    }
    .write(HFileKijiOutput
      .builder
      .withTableURI(args("output"))
      .withHFileOutput("%s/ratings".format(args("hfile-output")))
      .withTimestampField('timestamp)
      .withColumnSpecs(Map('ratingRecord -> QualifiedColumnOutputSpec.builder
        .withColumn("interactions", "ratings")
        .build,
        'decayedRatingRecord -> QualifiedColumnOutputSpec.builder
          .withColumn("interactions", "decayed_ratings")
          .build))
      .build)

  general_ratings
    .map('category -> 'entityId) { category: String =>
      EntityId(category)
    }
    .write(HFileKijiOutput
      .builder
      .withTableURI(args("category-output"))
      .withHFileOutput("%s/categories".format(args("hfile-output")))
      .withColumnSpecs(Map('num_ratings -> QualifiedColumnOutputSpec.builder
        .withColumn("stats", "num_ratings")
        .build,
        'sum_ratings -> QualifiedColumnOutputSpec.builder
          .withColumn("stats", "sum_ratings")
          .build,
        'sum_sq_rating -> QualifiedColumnOutputSpec.builder
          .withColumn("stats", "sum_squared_ratings")
          .build))
      .build)

  movie_tags.map(('movie_id, 'tag, 'cats) -> 'tagRecord) { tuple: (Long, String, String) =>
    val (movieId, tag, cats) = tuple
    MovieTag
      .newBuilder()
      .setMovieId(movieId)
      .setTag(tag)
      .setMovieCats(cats.split("\\|").toList.asJava)
      .build()
  }
    .write(HFileKijiOutput
      .builder
      .withTableURI(args("output"))
      .withHFileOutput("%s/tags".format(args("hfile-output")))
      .withTimestampField('timestamp)
      .withColumnSpecs(Map('tagRecord -> QualifiedColumnOutputSpec.builder
        .withColumn("interactions", "tags")
        .build))
      .build)

  private def ratingsToRecord(tuple: (Long, String, Float)): MovieRating = {
    val (movieId, cats, rating) = tuple
    MovieRating
      .newBuilder()
      .setMovieId(movieId)
      .setRating(rating.toDouble)
      .setMovieCats(cats.split("\\|").toList.asJava)
      .build()
  }
}

