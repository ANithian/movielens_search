package org.hokiesuns.movielens.loader

import akka.actor.ActorRef
import akka.actor.Terminated
import akka.actor.Actor
import scala.collection.mutable.ArrayBuffer
import org.apache.solr.client.solrj.SolrServer

object Reaper {
  // Used by others to register an Actor for watching
  case class WatchMe(ref: ActorRef)
}

class Reaper() extends Actor {
  import Reaper._

  // Keep track of what we're watching
  val watched = ArrayBuffer.empty[ActorRef]

  // Watch and check for termination
  final def receive = {
    case WatchMe(ref) =>
      context.watch(ref)
      watched += ref
    case Terminated(ref) =>
      watched -= ref
      if (watched.isEmpty) {
        println("Shutting down actors..")
        context.system.shutdown
      }
  }
}