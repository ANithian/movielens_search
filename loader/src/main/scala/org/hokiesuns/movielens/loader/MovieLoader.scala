package org.hokiesuns.movielens.loader

import com.fasterxml.jackson.databind.ObjectMapper
import java.io.File
import org.apache.solr.common.SolrInputDocument
import java.text.SimpleDateFormat
import scala.collection.mutable.ListBuffer
import org.apache.solr.client.solrj.impl.HttpSolrServer
import scala.collection.JavaConverters._
import scala.io.Source
import scala.collection.mutable.Map
import org.kiji.schema.KijiFactory
import org.kiji.schema.Kiji
import org.kiji.schema.KijiURI
import org.apache.avro.generic.GenericData
import org.apache.avro.Schema

object MovieLoader {

  def main(args: Array[String]): Unit = {
    val mapper = new ObjectMapper
    val ratingsMap = mapper.readValue(new File("input/movie_ratings.json"), classOf[java.util.HashMap[String, Float]]).asScala
    val decayedRatingsMap = readDecayedRatings
    val movieNode = mapper.readTree(new File("input/movies.json")).get("movies")
    val docList = ListBuffer[SolrInputDocument]()

    val uri = KijiURI.newBuilder("kiji://localhost/default").build()
    val kiji = Kiji.Factory.open(uri)
    val table = kiji.openTable("movies")
    val bufferedWriter = table.getWriterFactory().openBufferedWriter()

    /*
     *       "movie_id" -> id,
      "movie_title" -> title,
      "tags" -> tags.asJava,
      "description" -> desc,
      "categories" -> cats,
      "actors" -> actors)
     */
    val dateFormat = new SimpleDateFormat("yyyy-MM-dd")
    val solrServer = new HttpSolrServer("http://localhost:8080/movielens/")

    solrServer.deleteByQuery("id: [* TO *]")
    solrServer.commit()
    solrServer.optimize()
    for (i <- 0 until movieNode.size()) {

      val solrDoc = new SolrInputDocument
      val movie = movieNode.get(i)

      val movieId = movie.get("movie_id").asText()
      val eid = table.getEntityId(movieId.toLong.asInstanceOf[java.lang.Long])

      val title = movie.get("movie_title").asText()
      val cats = movie.get("categories")
      val actors = movie.get("actors")
      val tags = movie.get("tags")

      val rating = ratingsMap.get(movieId)
      val decayedStats = decayedRatingsMap.get(movieId)
      if (decayedStats.isDefined) {
        solrDoc.addField("decayed_rating", decayedStats.get._2)
        solrDoc.addField("num_ratings", decayedStats.get._1)
      }

      val lastSpace = title.lastIndexOf(" ")
      val newTitle = title.substring(0, lastSpace)
      val year = title
        .substring(lastSpace + 1, title.size)
        .replaceAll("\\(", "")
        .replaceAll("\\)", "")
        .toInt
      val releaseSeconds = dateFormat.parse(year + "-01-01").getTime() / 1000

      solrDoc.addField("id", movieId)
      solrDoc.addField("title", newTitle)
      solrDoc.addField("description", movie.get("description").asText())
      for (j <- 0 until actors.size()) {
        solrDoc.addField("actor", actors.get(j).asText())
      }
      val catArray = new GenericData.Array[String](cats.size(), Schema.createArray(Schema.create(Schema.Type.STRING)))

      for (j <- 0 until cats.size()) {
        solrDoc.addField("category", cats.get(j).asText())
        catArray.add(j, cats.get(j).asText())
      }
      bufferedWriter.put(eid, "info", "categories", catArray)

      for (j <- 0 until tags.size()) {
        solrDoc.addField("tag", tags.get(j).asText())
      }
      solrDoc.addField("release_seconds", releaseSeconds)
      if (rating.isDefined) {
        solrDoc.addField("avg_rating", rating.get)
      }

      docList.prepend(solrDoc)

      if (docList.size == 1000) {
        println("Loading..")
        solrServer.add(docList.asJava)
        docList.clear
        bufferedWriter.flush()
      }
    }
    println("Done.")
    if (docList.size > 0) {
      solrServer.add(docList.asJava)
    }
    solrServer.commit()
    solrServer.optimize()
    solrServer.shutdown()
    bufferedWriter.close()
    table.release()
    kiji.release()
  }

  def readDecayedRatings = {
    val ratingsMap = Map[String, (Long, Double)]()
    Source.fromFile(new File("input/decayed_ratings.tsv")).getLines.foreach({ line: String =>
      val parts = line.split("\\t")
      ratingsMap.put(parts(0), (parts(1).toLong, parts(2).toDouble))
    })
    ratingsMap
  }
}