package org.hokiesuns.movielens.loader

import scala.io.Source
import java.io.File
import scala.collection.immutable.Map
import scala.util.Sorting
import com.fasterxml.jackson.databind.ObjectMapper
import scala.collection.JavaConverters._
import com.twitter.scalding.Job
import com.twitter.scalding.Args
import com.twitter.scalding.TextLine
import scala.math.Numeric

class UserCategoryPrefsJob(args: Args) extends Job(args) {

  val MAX_TIMESTAMP = 1231131736L

  // I want to know for a given user, the num_ratings,avg_rating per movie category they have given.
  val movies = TextLine("input/movies.dat")
    .read
    //1::Toy Story (1995)::Adventure|Animation|Children|Comedy|Fantasy
    .mapTo('line -> ('movie_id, 'catList)) { line: String =>
      val parts = line.split("::")
      (parts(0).toLong, parts(2))
    }

  //1::122::5::838985046 (user, movie, rating, timestamp)
  val ratings = TextLine("input/ratings.dat")
    .read
    .mapTo('line -> ('user_id, 'movie_id, 'rating, 'timestamp)) { line: String =>
      val parts = line.split("::")
      (parts(0).toLong, parts(1).toLong, parts(2).toFloat, parts(3).toLong)
    }
    .joinWithSmaller(('movie_id, 'movie_id), movies)

  val user_ratings = ratings
    .flatMap('catList -> 'category) { catList: String => catList.split("\\|") }
    .groupBy(('user_id, 'category)) { _.sizeAveStdev('rating -> ('num_ratings, 'avg_rating, 'stddev_rating)) }
    .filter('num_ratings) { num_ratings: Int => num_ratings > 5 }
    .project('user_id, 'category, 'num_ratings, 'avg_rating)

  user_ratings.write(TextLine("output/user_rating_info"))

  val general_ratings = ratings
    .flatMap('catList -> 'category) { catList: String => catList.split("\\|") }
    .groupBy('category) { _.sizeAveStdev('rating -> ('num_ratings, 'avg_rating, 'stddev_rating)) }
    .filter('num_ratings) { num_ratings: Int => num_ratings > 5 }
    .project('category, 'num_ratings, 'avg_rating, 'stddev_rating)

  general_ratings.write(TextLine("output/general_rating_info"))
}