package org.hokiesuns.movielens.loader

import scala.io.Source
import java.io.File
import scala.collection.immutable.Map
import scala.util.Sorting
import com.fasterxml.jackson.databind.ObjectMapper
import scala.collection.JavaConverters._

object MovieRatingsSummarizer {

  val MAX_TIMESTAMP = 1231131736L

  def main(args: Array[String]): Unit = {
    val ratings = Source.fromFile(new File("input/ratings.dat"))
      .getLines
      .map(line => {
        val parts = line.split("::")
        val movie_id = parts(1).toInt
        val rating = parts(2).toFloat
        (movie_id, rating, 1)
      })
      .toSeq
      .groupBy(_._1)
      .map(kv => {
        val movie_id = kv._1
        val ratings = kv._2
        val reduced = ratings
          .reduce((result, current) => (0, current._2 + result._2, 1 + result._3))
        (movie_id -> (1.0 * reduced._2 / reduced._3).toFloat)
      })
    val mapper = new ObjectMapper
    mapper.writeValue(new File("ratings.json"), ratings.asJava)
  }
}