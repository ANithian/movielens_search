package org.hokiesuns.movielens.loader

import java.io.File
import java.io.FileWriter
import java.net.URLEncoder
import scala.collection.JavaConverters._
import scala.collection.mutable.Map
import scala.collection.mutable.Set
import scala.io.Source
import org.apache.http.client.methods.HttpGet
import org.apache.http.impl.client.DefaultHttpClient
import com.fasterxml.jackson.core.JsonFactory
import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.core.TreeNode
import com.fasterxml.jackson.databind.ObjectMapper
import akka.actor.Actor
import akka.actor.ActorSystem
import akka.actor.Props
import akka.routing.RoundRobinRouter

class MovieFetcherActor extends Actor {

  val baseUrl = "http://us.imdb.com/M/title-exact?"
  val baseOmdbUrl = "http://www.omdbapi.com/?plot=full"

  def receive = {
    case ParseMovie(movie) => {
      val writerActor = context.actorFor("../../writer")
      // Get the description from IMDB.
      val metadata = getDescription(movie.title)
      writerActor ! WriteMovie(movie, metadata._1, metadata._2)
    }
    case _ => println("parser received unknown message")
  }
  /*
   * <meta name="description" content="Directed by Joe Johnston.  With Robin Williams, Kirsten Dunst, Bonnie Hunt, Jonathan Hyde. When two kids find and play a magical board game, they release a man trapped for decades in it and a host of dangers that can only be stopped by finishing the game." />
   */
  def getDescription(title: String): (String, Array[String]) = {
    val urlString = baseUrl + URLEncoder.encode(title)
    val httpGet = new HttpGet(urlString)
    httpGet.setHeader("User-Agent", "Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.9.2.13) Gecko/20101206 Ubuntu/10.10 (maverick) Firefox/3.6.13")
    val httpClient = new DefaultHttpClient
    val response = httpClient.execute(httpGet)
    val source = Source.fromInputStream(response.getEntity().getContent())
    var description = ""
    var inActorList = false

    val actorNames = Set[String]()

    source.getLines.foreach(line => {
      if (line.contains("<meta name=\"description\" ")) {
        val idx = line.indexOf("content=\"")
        val eidx = line.lastIndexOf("\"")

        if (idx >= 0) {
          description = line.substring(idx + 9, eidx)
        }
      } else if (line.contains("<table class=\"cast_list\">")) {
        inActorList = true
      } else if (inActorList) {
        if (line.contains("</table>")) {
          inActorList = false
        } else if (line.contains("itemprop=\"name\"")) {
          // <span class="itemprop" itemprop="name">Robin Williams</span>
          val idx = line.indexOf("\">")
          val eidx = line.lastIndexOf("<")
          actorNames.add(line.substring(idx + 2, eidx))
        }
      }
    })
    source.close
    (description, actorNames.toArray)
  }

  def getMetadata(title: String): (String, Array[String]) = {
    val lastSpace = title.lastIndexOf(" ")
    val newTitle = title.substring(0, lastSpace)

    val urlString = baseOmdbUrl + "&t=" + URLEncoder.encode(newTitle)
    val httpGet = new HttpGet(urlString)
    httpGet.setHeader("User-Agent", "Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.9.2.13) Gecko/20101206 Ubuntu/10.10 (maverick) Firefox/3.6.13")
    val httpClient = new DefaultHttpClient
    val response = httpClient.execute(httpGet)

    val mapper = new ObjectMapper
    val node = mapper.readTree(response.getEntity().getContent())
    val actors = if (node.get("Actors") != null) {
      node.get("Actors").asText().split(",")
    } else {
      println("No actors for movie " + title)
      Array[String]()
    }
    val plot = if (node.get("Plot") != null) {
      node.get("Plot").asText()
    } else {
      println("No plot for movie " + title)
      ""
    }
    (plot, actors)
  }
}

class MovieWriterActor(val outputFile: File, val numMovies: Int) extends Actor {

  var totalDocs = 0
  val objMapper = new ObjectMapper()
  val outputWriter = new FileWriter(outputFile)
  val jsonGenerator = objMapper.getJsonFactory().createGenerator(outputWriter)

  jsonGenerator.writeStartObject()
  jsonGenerator.writeArrayFieldStart("movies")

  def receive = {
    case WriteMovie(movie, description, actors) => {
      val jsonNode: TreeNode = objMapper.valueToTree(movie.toMap(description, actors))
      //      objMapper.writeValue(outputWriter, movie.toMap(description))
      jsonGenerator.writeTree(jsonNode)
      jsonGenerator.useDefaultPrettyPrinter()
      totalDocs += 1
      if (totalDocs >= numMovies) {

        println("Counter shutting down..")
        jsonGenerator.writeEndArray()
        jsonGenerator.writeEndObject()
        jsonGenerator.flush()
        jsonGenerator.close()
        context.stop(self)
      }
    }
    case _ => println("writer received unknown message")
  }
}

case class ParseMovie(val movie: Movie)
case class WriteMovie(val movie: Movie, val description: String, val actorNames: Array[String])

case class Movie(val id: Int, val title: String, val tags: Set[String], val cats: Array[String]) {
  def toMap(desc: String, actors: Array[String]) = {
    val map = Map(
      "movie_id" -> id,
      "movie_title" -> title,
      "tags" -> tags.asJava,
      "description" -> desc,
      "categories" -> cats,
      "actors" -> actors)
    map.asJava
  }
}

object MovieLensEnhancer {

  def main(args: Array[String]): Unit = {
    val moviesFile = Source.fromFile(new File("input/movies.dat"))

    val tags = normalizeTags("input/tags.dat")
    val system = ActorSystem("MySystem")
    val reaper = system.actorOf(Props[Reaper])
    val writer = system.actorOf(Props(new MovieWriterActor(new File("output.json"), 10681)), "writer")

    reaper ! Reaper.WatchMe(writer)

    val actor = system.actorOf(Props[MovieFetcherActor].withRouter(RoundRobinRouter(50)).withDispatcher("parser-dispatcher"), "parser")

    moviesFile.getLines.foreach(line => {
      //1::Toy Story (1995)::Adventure|Animation|Children|Comedy|Fantasy
      val parts = line.split("::")
      val cats = parts(2).split("\\|")
      val mid = parts(0).toInt
      actor ! ParseMovie(Movie(mid, parts(1), tags.getOrElse(mid, Set[String]()), cats))
    })
  }

  def normalizeTags(infile: String): Map[Int, Set[String]] = {
    val file = Source.fromFile(new File(infile)).getLines.map(line => {
      val parts = line.split("::")
      val movieId = parts(1).toInt
      val tag = parts(2)
      (movieId, tag)
    }).toArray

    file.foldLeft(Map[Int, Set[String]]())((result, current) => {
      val tagList = result.getOrElse(current._1, Set[String]())
      tagList.add(current._2)

      result.put(current._1, tagList)
      result
    })
  }
}