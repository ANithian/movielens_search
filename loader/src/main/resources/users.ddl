CREATE TABLE 'users' WITH DESCRIPTION 'User Movie Ratings Database'
ROW KEY FORMAT (user_id LONG)
WITH LOCALITY GROUP default WITH DESCRIPTION 'main storage' (
  MAXVERSIONS = INFINITY,
  TTL = FOREVER,
  INMEMORY = false,
  COMPRESSED WITH GZIP,
  FAMILY interactions (
    tags CLASS org.hokiesuns.movielens.avro.MovieTag WITH DESCRIPTION 'Tags that the user has assigned to movie',
    ratings CLASS org.hokiesuns.movielens.avro.MovieRating WITH DESCRIPTION 'Ratings that the user has given to movies',
    decayed_ratings CLASS org.hokiesuns.movielens.avro.MovieRating WITH DESCRIPTION 'Decayed Ratings. Not sure if necessary.',
    clicks CLASS org.hokiesuns.movielens.avro.MovieRating WITH DESCRIPTION 'Movies the user has clicked on',
    watches CLASS org.hokiesuns.movielens.avro.MovieRating WITH DESCRIPTION 'Movies the user has watched'
  )
),
LOCALITY GROUP inmemory WITH DESCRIPTION 'main storage' (
  MAXVERSIONS = 1,
  TTL = FOREVER,
  INMEMORY = true,
  COMPRESSED WITH GZIP,
  MAP TYPE FAMILY boosts CLASS org.hokiesuns.movielens.avro.SearchBoosts WITH DESCRIPTION 'Boosts for this user. Qualifier is model_name'  
);