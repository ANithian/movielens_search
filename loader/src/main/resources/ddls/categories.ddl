CREATE TABLE 'categories' WITH DESCRIPTION 'User Movie Ratings Database'
ROW KEY FORMAT (cat_name STRING)
WITH LOCALITY GROUP default WITH DESCRIPTION 'main storage' (
  MAXVERSIONS = INFINITY,
  TTL = FOREVER,
  INMEMORY = false,
  COMPRESSED WITH GZIP,
  FAMILY stats (
    num_ratings COUNTER WITH DESCRIPTION 'Number of Ratings',
    sum_ratings COUNTER WITH DESCRIPTION 'Sum of ratings',
    sum_squared_ratings COUNTER WITH DESCRIPTION 'Sum Squared of Ratings'
  )
),
LOCALITY GROUP inmemory WITH DESCRIPTION 'main storage' (
  MAXVERSIONS = 1,
  TTL = FOREVER,
  INMEMORY = true,
  COMPRESSED WITH GZIP,
  FAMILY derived_stats (
    ratings CLASS org.hokiesuns.movielens.avro.MovieStats WITH DESCRIPTION 'Avg Rating'
  )
);