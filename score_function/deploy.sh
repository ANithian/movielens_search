#!/bin/bash

export KIJI_CLASSPATH=${KIJI_HOME}/model-repo/lib/*
export KIJI=${KIJI:-"kiji://.env/movies"}

kiji ls ${KIJI} | grep "model_repo"
if [ $? -ne 0 ]; then
  kiji model-repo init --kiji=${KIJI} file:///tmp/model_repo
fi

mvn clean package -DskipTests
MOVIE_JAR=$(find target -maxdepth 1 -name "*-SNAPSHOT.jar")

kiji model-repo deploy org.hokiesuns.movielens.search ${MOVIE_JAR} \
--model-container=conf/model_container.json \
--message="Initial upload" \
--deps=pom.xml \
--deps-resolver=maven \
--kiji=${KIJI} \
--production-ready=true