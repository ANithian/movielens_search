package org.hokiesuns.movielens.scoring;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import com.google.common.collect.Maps;

import org.hokiesuns.movielens.avro.MovieRating;
import org.hokiesuns.movielens.avro.SearchBoosts;

import org.kiji.mapreduce.kvstore.KeyValueStore;
import org.kiji.schema.EntityId;
import org.kiji.schema.Kiji;
import org.kiji.schema.KijiCell;
import org.kiji.schema.KijiDataRequest;
import org.kiji.schema.KijiDataRequestBuilder.ColumnsDef;
import org.kiji.schema.KijiRowData;
import org.kiji.schema.KijiTable;
import org.kiji.schema.KijiTableReader;
import org.kiji.schema.KijiURI;
import org.kiji.scoring.FreshenerContext;
import org.kiji.scoring.FreshenerGetStoresContext;
import org.kiji.scoring.FreshenerSetupContext;
import org.kiji.scoring.ScoreFunction;

public class SearchScoringFunction extends ScoreFunction<SearchBoosts> {

  private Kiji mKiji = null;
  private KijiTable mKijiTable = null;
  private KijiTableReader mCategoryReader = null;
  private KijiDataRequest mCategoryRequest = null;

  public static class CategoryBoost implements Comparable<CategoryBoost> {

    private String category;
    private int numObs;
    private float totalScore;

    private double zScore;

    public CategoryBoost(String category) {
      super();
      this.category = category;
    }

    public void setZScore(double zscore) {
      this.zScore = zscore;
    }

    public float getAverageRating() {
      return totalScore / numObs;
    }

    public void submitRating(float rating) {
      numObs++;
      totalScore += rating;
    }

    @Override
    public int compareTo(CategoryBoost arg0) {
      // TODO Auto-generated method stub
      return Double.compare(Math.abs(this.zScore), Math.abs(arg0.zScore)) * -1;
    }

    @Override
    public String toString() {
      return "CategoryBoost [category=" + category + ", numObs=" + numObs + ", totalScore="
          + totalScore + ", zScore=" + zScore + "]";
    }
  }

  @Override
  public KijiDataRequest getDataRequest(FreshenerContext arg0) throws IOException {
    // TODO Auto-generated method stub
    return KijiDataRequest
        .builder()
        .addColumns(ColumnsDef
            .create()
            .withMaxVersions(10)
            .add("interactions", "ratings")
        )
        .build();
  }

  @Override
  public org.kiji.scoring.ScoreFunction.TimestampedValue<SearchBoosts> score(KijiRowData row,
      FreshenerContext context) throws IOException {
    int iNumToReturn = 3;
    List<CategoryBoost> sortedBoosts = null;
    Map<String, String> boostParams = Maps.newHashMap();

    String sNumToReturn = context.getParameter("numToReturn");
    if (sNumToReturn != null) {
      iNumToReturn = Integer.parseInt(sNumToReturn);
    }

    Map<String, CategoryBoost> boostsMap = Maps.newHashMap();


    int iNumObs = 0;
    Iterable<KijiCell<MovieRating>> it = row.asIterable("interactions", "ratings");
    for (KijiCell<MovieRating> r : it) {
      MovieRating rating = r.getData();
      for (String cat : rating.getMovieCats()) {
        CategoryBoost boost = boostsMap.get(cat);
        if (boost == null) {
          boost = new CategoryBoost(cat);
          boostsMap.put(cat, boost);
        }
        boost.submitRating(rating.getRating().floatValue());
        iNumObs++;
      }
    }

    if(boostsMap.size() > 0) {
      // Now go through each category and compute the z-score
      for (CategoryBoost cat : boostsMap.values()) {
        EntityId eid = mKijiTable.getEntityId(cat.category);
        KijiRowData categoryData = mCategoryReader.get(eid, mCategoryRequest);
        Long numRatings = categoryData.getMostRecentValue("stats", "num_ratings");
        Long sumRatings = categoryData.getMostRecentValue("stats", "sum_ratings");
        Long sumSquaredRatings = categoryData.getMostRecentValue("stats", "sum_squared_ratings");

        double globalCatAvg = (sumRatings * 1.0 / numRatings);
        double globalCatStdDev = Math.sqrt((sumSquaredRatings * 1.0/numRatings) - globalCatAvg * globalCatAvg);

        double zScore = ((cat.getAverageRating() - globalCatAvg) / globalCatStdDev)
            * (cat.numObs * 1.0f / iNumObs * 1.0f);
        cat.setZScore(zScore);
      }

      List<CategoryBoost> boosts = new ArrayList<CategoryBoost>(boostsMap.values());
      Collections.sort(boosts);
      if (iNumToReturn > boosts.size()) {
        sortedBoosts = boosts;
      } else {
        sortedBoosts = boosts.subList(0, iNumToReturn);
      }

      StringBuilder boostBuilder = new StringBuilder();
      boostBuilder.append("sum(1,");
      boostBuilder.append("sum(");
      for(int i=0; i < sortedBoosts.size(); i++) {
        CategoryBoost boost = sortedBoosts.get(i);
        // Set the category name params.
        boostParams.put("cat" + (i+1), "category:" + boost.category);
        //sum( product(query($cat1),1.482),product(query($cat2),0.1199),product(query($cat3),1.448) )
        boostBuilder.append("product(query($cat" + (i+1) + ")," + boost.zScore + ")");
        if(i < boosts.size() - 1) {
          boostBuilder.append(",");
        }
      }
      boostBuilder.append("))");
      boostParams.put("b",boostBuilder.toString());
      boostParams.put("q", "{!boost b=$b defType=edismax v=$qq}");
    }
    return TimestampedValue.create(SearchBoosts.newBuilder().setBoosts(boostParams).build());
  }

  @Override
  public Map<String, KeyValueStore<?, ?>> getRequiredStores(FreshenerGetStoresContext context) {
    // TODO Auto-generated method stub
    return super.getRequiredStores(context);
  }

  @Override
  public void cleanup(FreshenerSetupContext context) throws IOException {
    // TODO Auto-generated method stub
    super.cleanup(context);
    mCategoryReader.close();
    mKiji.release();
  }

  @Override
  public void setup(FreshenerSetupContext context) throws IOException {
    super.setup(context);
    String kijiUri = context.getParameter("category.uri");
    mKiji = Kiji.Factory.open(KijiURI.newBuilder(kijiUri).build());
    mKijiTable = mKiji.openTable("categories");
    mCategoryReader = mKijiTable.openTableReader();
    mCategoryRequest = KijiDataRequest
        .builder()
        .addColumns(ColumnsDef.create().addFamily("stats"))
        .build();
  }
}