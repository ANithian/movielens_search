/**
 * Autogenerated by Avro
 * 
 * DO NOT EDIT DIRECTLY
 */
package org.hokiesuns.movielens.avro;  
@SuppressWarnings("all")
@org.apache.avro.specific.AvroGenerated
public class MovieTag extends org.apache.avro.specific.SpecificRecordBase implements org.apache.avro.specific.SpecificRecord {
  public static final org.apache.avro.Schema SCHEMA$ = new org.apache.avro.Schema.Parser().parse("{\"type\":\"record\",\"name\":\"MovieTag\",\"namespace\":\"org.hokiesuns.movielens.avro\",\"fields\":[{\"name\":\"movie_id\",\"type\":\"long\"},{\"name\":\"tag\",\"type\":{\"type\":\"string\",\"avro.java.string\":\"String\"}},{\"name\":\"movie_cats\",\"type\":{\"type\":\"array\",\"items\":{\"type\":\"string\",\"avro.java.string\":\"String\"}}}]}");
  public static org.apache.avro.Schema getClassSchema() { return SCHEMA$; }
  @Deprecated public long movie_id;
  @Deprecated public java.lang.String tag;
  @Deprecated public java.util.List<java.lang.String> movie_cats;

  /**
   * Default constructor.  Note that this does not initialize fields
   * to their default values from the schema.  If that is desired then
   * one should use {@link \#newBuilder()}. 
   */
  public MovieTag() {}

  /**
   * All-args constructor.
   */
  public MovieTag(java.lang.Long movie_id, java.lang.String tag, java.util.List<java.lang.String> movie_cats) {
    this.movie_id = movie_id;
    this.tag = tag;
    this.movie_cats = movie_cats;
  }

  public org.apache.avro.Schema getSchema() { return SCHEMA$; }
  // Used by DatumWriter.  Applications should not call. 
  public java.lang.Object get(int field$) {
    switch (field$) {
    case 0: return movie_id;
    case 1: return tag;
    case 2: return movie_cats;
    default: throw new org.apache.avro.AvroRuntimeException("Bad index");
    }
  }
  // Used by DatumReader.  Applications should not call. 
  @SuppressWarnings(value="unchecked")
  public void put(int field$, java.lang.Object value$) {
    switch (field$) {
    case 0: movie_id = (java.lang.Long)value$; break;
    case 1: tag = (java.lang.String)value$; break;
    case 2: movie_cats = (java.util.List<java.lang.String>)value$; break;
    default: throw new org.apache.avro.AvroRuntimeException("Bad index");
    }
  }

  /**
   * Gets the value of the 'movie_id' field.
   */
  public java.lang.Long getMovieId() {
    return movie_id;
  }

  /**
   * Sets the value of the 'movie_id' field.
   * @param value the value to set.
   */
  public void setMovieId(java.lang.Long value) {
    this.movie_id = value;
  }

  /**
   * Gets the value of the 'tag' field.
   */
  public java.lang.String getTag() {
    return tag;
  }

  /**
   * Sets the value of the 'tag' field.
   * @param value the value to set.
   */
  public void setTag(java.lang.String value) {
    this.tag = value;
  }

  /**
   * Gets the value of the 'movie_cats' field.
   */
  public java.util.List<java.lang.String> getMovieCats() {
    return movie_cats;
  }

  /**
   * Sets the value of the 'movie_cats' field.
   * @param value the value to set.
   */
  public void setMovieCats(java.util.List<java.lang.String> value) {
    this.movie_cats = value;
  }

  /** Creates a new MovieTag RecordBuilder */
  public static org.hokiesuns.movielens.avro.MovieTag.Builder newBuilder() {
    return new org.hokiesuns.movielens.avro.MovieTag.Builder();
  }
  
  /** Creates a new MovieTag RecordBuilder by copying an existing Builder */
  public static org.hokiesuns.movielens.avro.MovieTag.Builder newBuilder(org.hokiesuns.movielens.avro.MovieTag.Builder other) {
    return new org.hokiesuns.movielens.avro.MovieTag.Builder(other);
  }
  
  /** Creates a new MovieTag RecordBuilder by copying an existing MovieTag instance */
  public static org.hokiesuns.movielens.avro.MovieTag.Builder newBuilder(org.hokiesuns.movielens.avro.MovieTag other) {
    return new org.hokiesuns.movielens.avro.MovieTag.Builder(other);
  }
  
  /**
   * RecordBuilder for MovieTag instances.
   */
  public static class Builder extends org.apache.avro.specific.SpecificRecordBuilderBase<MovieTag>
    implements org.apache.avro.data.RecordBuilder<MovieTag> {

    private long movie_id;
    private java.lang.String tag;
    private java.util.List<java.lang.String> movie_cats;

    /** Creates a new Builder */
    private Builder() {
      super(org.hokiesuns.movielens.avro.MovieTag.SCHEMA$);
    }
    
    /** Creates a Builder by copying an existing Builder */
    private Builder(org.hokiesuns.movielens.avro.MovieTag.Builder other) {
      super(other);
      if (isValidValue(fields()[0], other.movie_id)) {
        this.movie_id = data().deepCopy(fields()[0].schema(), other.movie_id);
        fieldSetFlags()[0] = true;
      }
      if (isValidValue(fields()[1], other.tag)) {
        this.tag = data().deepCopy(fields()[1].schema(), other.tag);
        fieldSetFlags()[1] = true;
      }
      if (isValidValue(fields()[2], other.movie_cats)) {
        this.movie_cats = data().deepCopy(fields()[2].schema(), other.movie_cats);
        fieldSetFlags()[2] = true;
      }
    }
    
    /** Creates a Builder by copying an existing MovieTag instance */
    private Builder(org.hokiesuns.movielens.avro.MovieTag other) {
            super(org.hokiesuns.movielens.avro.MovieTag.SCHEMA$);
      if (isValidValue(fields()[0], other.movie_id)) {
        this.movie_id = data().deepCopy(fields()[0].schema(), other.movie_id);
        fieldSetFlags()[0] = true;
      }
      if (isValidValue(fields()[1], other.tag)) {
        this.tag = data().deepCopy(fields()[1].schema(), other.tag);
        fieldSetFlags()[1] = true;
      }
      if (isValidValue(fields()[2], other.movie_cats)) {
        this.movie_cats = data().deepCopy(fields()[2].schema(), other.movie_cats);
        fieldSetFlags()[2] = true;
      }
    }

    /** Gets the value of the 'movie_id' field */
    public java.lang.Long getMovieId() {
      return movie_id;
    }
    
    /** Sets the value of the 'movie_id' field */
    public org.hokiesuns.movielens.avro.MovieTag.Builder setMovieId(long value) {
      validate(fields()[0], value);
      this.movie_id = value;
      fieldSetFlags()[0] = true;
      return this; 
    }
    
    /** Checks whether the 'movie_id' field has been set */
    public boolean hasMovieId() {
      return fieldSetFlags()[0];
    }
    
    /** Clears the value of the 'movie_id' field */
    public org.hokiesuns.movielens.avro.MovieTag.Builder clearMovieId() {
      fieldSetFlags()[0] = false;
      return this;
    }

    /** Gets the value of the 'tag' field */
    public java.lang.String getTag() {
      return tag;
    }
    
    /** Sets the value of the 'tag' field */
    public org.hokiesuns.movielens.avro.MovieTag.Builder setTag(java.lang.String value) {
      validate(fields()[1], value);
      this.tag = value;
      fieldSetFlags()[1] = true;
      return this; 
    }
    
    /** Checks whether the 'tag' field has been set */
    public boolean hasTag() {
      return fieldSetFlags()[1];
    }
    
    /** Clears the value of the 'tag' field */
    public org.hokiesuns.movielens.avro.MovieTag.Builder clearTag() {
      tag = null;
      fieldSetFlags()[1] = false;
      return this;
    }

    /** Gets the value of the 'movie_cats' field */
    public java.util.List<java.lang.String> getMovieCats() {
      return movie_cats;
    }
    
    /** Sets the value of the 'movie_cats' field */
    public org.hokiesuns.movielens.avro.MovieTag.Builder setMovieCats(java.util.List<java.lang.String> value) {
      validate(fields()[2], value);
      this.movie_cats = value;
      fieldSetFlags()[2] = true;
      return this; 
    }
    
    /** Checks whether the 'movie_cats' field has been set */
    public boolean hasMovieCats() {
      return fieldSetFlags()[2];
    }
    
    /** Clears the value of the 'movie_cats' field */
    public org.hokiesuns.movielens.avro.MovieTag.Builder clearMovieCats() {
      movie_cats = null;
      fieldSetFlags()[2] = false;
      return this;
    }

    @Override
    public MovieTag build() {
      try {
        MovieTag record = new MovieTag();
        record.movie_id = fieldSetFlags()[0] ? this.movie_id : (java.lang.Long) defaultValue(fields()[0]);
        record.tag = fieldSetFlags()[1] ? this.tag : (java.lang.String) defaultValue(fields()[1]);
        record.movie_cats = fieldSetFlags()[2] ? this.movie_cats : (java.util.List<java.lang.String>) defaultValue(fields()[2]);
        return record;
      } catch (Exception e) {
        throw new org.apache.avro.AvroRuntimeException(e);
      }
    }
  }
}
